ALLOWED_HOSTS = ['*']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'retail_db',
        'USER': 'retail',
        'PASSWORD': 'retail',
        'HOST': 'localhost',
        'PORT': '',
    }
}