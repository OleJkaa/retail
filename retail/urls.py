from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from retail import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^cart/', include('cart.urls', namespace='cart')),
    path('', include('social_django.urls')),
    url(r'^', include('cosmetic.urls', namespace='cosmetic')),
    path('summernote/', include('django_summernote.urls')),  # django_summernote URLS
]
urlpatterns  += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
