from django.db import models
from taggit.managers import TaggableManager
from django.conf import settings
# Create your models here.

app_name = 'cosmetic'

def category_picture_directory_path(instance, filename):
    return 'images/category/{0}/{1}'.format(instance.slug, filename)


def product_picture_directory_path(instance, filename):
    return 'images/products/{0}/{1}'.format(instance.slug, filename)


class UserProfile(models.Model):
    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профиль пользователей'

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True, related_name='user_profile', verbose_name='Профиль пользователя')
    middle_name = models.CharField(max_length=64, verbose_name='Отчество', blank=True, null=True)
    phone = models.CharField(verbose_name='Номер телефона', max_length=32, blank=True, null=True)

    def __repr__(self):
        return '{} {} {}'.format(self.user.email, self.user.last_name, self.user.first_name)

class Category(models.Model):
    class Meta:
        verbose_name = 'Категория товара'
        verbose_name_plural = 'Категории товаров'

    category_product = models.CharField(verbose_name='Категория продукта', max_length=100)
    index = models.SmallIntegerField(verbose_name='Порядковый номер', default=1)
    slug = models.SlugField(verbose_name='Адрес страницы Slug')
    image = models.FileField(upload_to=category_picture_directory_path, verbose_name='Изображение')
    created = models.DateTimeField(auto_now=False, auto_now_add=True, verbose_name='Дата создания')
    changed = models.DateTimeField(auto_now=True, auto_now_add=False, verbose_name='Дата редактирования')

    def __str__(self):
        return self.category_product

class Product(models.Model):
    class Meta:
        verbose_name = 'Карточка продукта'
        verbose_name_plural = 'Карточки продуктов'

    name = models.CharField(verbose_name='Наименование продукта', max_length=250)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, verbose_name='Категория товара',
                                 related_name='all_product')
    slug = models.SlugField(verbose_name='Адрес страницы Slug')
    brand = models.CharField(verbose_name='Бренд', max_length=200)
    country = models.CharField(verbose_name='Страна производитель', max_length=200)
    manufacturer = models.CharField(verbose_name='Производитель', max_length=200)
    size = models.CharField(verbose_name='объем', max_length=200)
    method_of_application = models.TextField(verbose_name='Способ применения')
    description = models.TextField(verbose_name='Описание товара')
    status = models.CharField(verbose_name='Статус продукта', max_length=100, blank=True, null=True,choices=(
        ('new','Новинка'),
        ('hot','Популярная'),
        ('sale','Распродажа')
    ))
    image_1 = models.FileField(upload_to=product_picture_directory_path, verbose_name='Изображение 1')
    image_2 = models.FileField(upload_to=product_picture_directory_path, verbose_name='Изображение 2', blank=True, null=True)
    image_3 = models.FileField(upload_to=product_picture_directory_path, verbose_name='Изображение 3', blank=True, null=True)
    image_4 = models.FileField(upload_to=product_picture_directory_path, verbose_name='Изображение 4', blank=True, null=True)
    image_5 = models.FileField(upload_to=product_picture_directory_path, verbose_name='Изображение 5', blank=True, null=True)
    image_6 = models.FileField(upload_to=product_picture_directory_path, verbose_name='Изображение 6', blank=True, null=True)
    current_price = models.DecimalField(verbose_name='Текущая цена продукта', max_digits=32, decimal_places=2)
    sale_price = models.DecimalField(verbose_name='Акционная цена', max_digits=32, decimal_places=2, blank=True, null=True)
    end_sale = models.DateTimeField(verbose_name='Окончание акционной цены', blank=True, null=True)
    number_sold = models.IntegerField(verbose_name='Количество проданных', default=0)
    number_in_stock = models.IntegerField(verbose_name='Количество на складе', default=0)

    tags = TaggableManager()
    created = models.DateTimeField(auto_now=False, auto_now_add=True, verbose_name='Дата создания')
    changed = models.DateTimeField(auto_now=True, auto_now_add=False, verbose_name='Дата редактирования')

    def __str__(self):
        return "{}. Цена: {}".format(self.name, self.current_price)


    def get_absolute_url(self):
        return "/{}/{}".format(self.category.slug, self.slug)
