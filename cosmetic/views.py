from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, DetailView

from cart.forms import CartAddProductForm
from cosmetic.forms import LoginForm, RegisterForm
from cosmetic.models import Product, UserProfile


class HomePage(TemplateView):
    template_name = 'html/home.html'


    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['many_people_like_objects'] = Product.objects.all()[:3]
        context['many_people_like_objects'] = Product.objects.all()[:3]
        return context

class ProductView(DetailView):
    template_name = 'html/product_view.html'
    slug_url_kwarg = 'slug'
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cart_product_form'] = CartAddProductForm()
        return context


class Login(TemplateView):
    template_name = 'html/sign-in/sign-in.html'

    def post(self, request, *args, **kwargs):
        if request.GET.get('submit', None) and request.GET.get('submit', None) == 'register':
            register_error = list()
            user = User()
            email = request.POST.get('exampleInputEmail', None)
            password1 = request.POST.get('exampleInputPassword1', None)
            password2 = request.POST.get('exampleInputPassword2', None)
            first_name = request.POST.get('exampleInputName', None)
            last_name = request.POST.get('exampleInputLastName', None)
            middle_name = request.POST.get('exampleInputMiddleName', None)
            phone = request.POST.get('exampleInputPhone', None)
            if email and not User.objects.filter(email=email).exists():
                user.email = email
                user.username = email
            else:
                register_error.append('Данный email уже зарегистрирован')
            if password1 == password2:
                user.password = password1
            else:
                register_error.append('Пароли не совпадают')
            if first_name:
                user.first_name = first_name
            if last_name:
                user.last_name = last_name
            if phone and UserProfile.objects.filter(phone=phone).exists():
                register_error.append('Данный телефон уже зарегистрирован')
            if not register_error:
                user.is_active = True
                user.save()
                login(request, user)
                user_profile = user.user_profile.create()
                if phone:
                    user_profile.phone = phone
                if middle_name:
                    user_profile.middle_name = middle_name
                user_profile.save()
            else:
                kwargs.update({'register_error': register_error})
                kwargs.update({'user_form': RegisterForm(initial={
                    'exampleInputEmail': email,
                    'exampleInputName': first_name,
                    'exampleInputLastName': last_name,
                    'exampleInputMiddleName': middle_name,
                    'exampleInputPhone': phone
                })})
        elif request.GET.get('submit', None) and request.GET.get('submit', None) == 'login':
            form = LoginForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                user = authenticate(username=cd['exampleInputEmail1'], password=cd['exampleInputPassword1'])
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        kwargs.update({'successfully': 'Авторизация прошла успешно'})
                    else:
                        kwargs.update({'error': 'Аккаунт отключен'})
                else:
                    kwargs.update({'error': 'Неверный логин или пароль'})
        return super().get(request, *args, **kwargs)
