from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import logout_then_login
from django.urls import path, include

from cosmetic import views
from cosmetic.views import HomePage
from retail import settings

app_name = 'cosmetic'
urlpatterns = [
    url('^$', HomePage.as_view(), name='home_page'),
    # url(r'^(?P<category_slug>[-\w]+)/$', views.product_list, name='product_list_by_category'),
    # url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.product_detail, name='product_detail'),
    url(r'^(?P<category_slug>[-\w]+)/(?P<slug>[-\w]+)/$', views.ProductView.as_view(), name='product_detail'),
    url(r'^login', views.Login.as_view(), name='login'),
    url(r'^logout/', logout_then_login, name='logout'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)