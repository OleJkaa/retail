from django import forms


#  Форма авторизации пользователя на сайте
class LoginForm(forms.Form):
    exampleInputEmail1 = forms.EmailField()
    exampleInputPassword1 = forms.CharField(widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    exampleInputEmail = forms.CharField(required=False)
    exampleInputPassword1 = forms.CharField(required=False)
    exampleInputPassword2 = forms.CharField(required=False)
    exampleInputName = forms.CharField(required=False)
    exampleInputLastName = forms.CharField(required=False)
    exampleInputMiddleName = forms.CharField(required=False)
    exampleInputPhone = forms.CharField(required=False)