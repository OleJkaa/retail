from django.contrib import admin

# Register your models here.
from django_summernote.admin import SummernoteModelAdmin

from cosmetic.models import Product, Category


class ProductAdmin(SummernoteModelAdmin):
    list_display = ('name', 'category', 'current_price', 'number_sold', 'number_in_stock')
    list_filter = ('category',)
    search_fields = ('name', 'category', 'current_price', 'number_sold', 'number_in_stock')
    date_hierarchy = 'created'
    prepopulated_fields = {"slug": ('name',)}
    summer_note_fields = ('method_of_application', 'description')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('category_product', 'index')
    list_filter = ('category_product',)
    search_fields = ('category_product',)
    date_hierarchy = 'created'
    prepopulated_fields = {"slug": ('category_product',)}


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
